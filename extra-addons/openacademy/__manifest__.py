# -*- coding: utf-8 -*-
{
    'name': "Open Academy with veeru",

    'summary': """Manage trainings""",

    'description': """
        Open Academy module for managing trainings:
            - training courses
            - training sessions
            - attendees registration
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",


    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'data/data.xml',
        'views/openacademy.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}
